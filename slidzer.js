(function($, window, document){
    $.fn.slidzer = function(params) {
        var currentSlidee = 1;                              // Now active slide image
        var slide_chooser = null;                           // buttons panel slide wraper class
        var main_slider = $(this);                          // main sliders class
        var slid = $(this).find('.indslider');                         // image slaiders wrap
        var sliderss = $(this).find('.it');                            // images sliders content
        var buttons_panel_view = params.slide_buttons_panel;       // if buttons panel is activated: true or false
        var maxSliderss = $(this).find('div.it').length - 1;           // how much slides in total
        var is_background = false;                          // if background enabled under slide image: false or true.
        var speed = params.slide_speed;                            // slide changer speed
        var slided_counter = 1;                             // counts every slide change
        var is_slide_urls = params.slide_urls_allow;               // false or true if you whant to allow click on slide image and redirect to page

        // -- ANIMATIONS --
        // -- fadeto: fade slides
        // -- none: no animation
        // -- slide: left -right
        var animation = params.slide_animation;                    // slider module animation type

        // -- BLOCK SETUP --
        //var block_slide_width = params.block_width + 'px';         // slider module block width in px
        //var block_slide_height = params.block_height + 'px';       // slider module block height in px
        var block_slide_height = 'auto';                                

        var block_slide_width = (params.block_width / 244) * 100  + '%';         // slider module block width in %, 244 - default width in px
       // var block_slide_height = (params.block_height / 144) * 100  + '%';       // slider module block height in %, 144 - default width in px
        
        var block_slide_numb_width = params.block_width;           // slider module block width in integer
        var full_block_slide_width = block_slide_numb_width * maxSliderss;  // all sliders overall width.Requared for slide animation

        var interval = null;    // set interval object

        setUpEnvironment();     // SET UP configurations needed to sliders module
        setUpButtonsPanel();    // SET UP buttons panel if it is enabled
        setUpCurrentSlide();    // SET UP current slide. It is requered if current slide is not a first one!!!
        loadSlideModule();      // STARTS SLIDE MODULE TO ACTION

        function loadSlideModule () {
            interval = setInterval(changeSlide, speed);
        }

        function changeSlide () {
            if (currentSlidee > maxSliderss) {
                currentSlidee = 0;
            }

            if (currentSlidee == 0) {
                $($(sliderss)[maxSliderss]).attr('class', 'it');
                $($(sliderss)[currentSlidee]).attr('class', 'it slider_active');

                if (animation == "fadeto") {
                    $($(sliderss)[currentSlidee]).css("opacity", "0");
                    $($(sliderss)[currentSlidee]).fadeTo("slow", 1, function () {
                        //action
                    });
                } else if (animation == "slide") {
                    if (slided_counter != 1) {
                        $($(slid)).animate({"right": '-=' + (full_block_slide_width)});
                    }
                } else if (animation == "none") {
                    //none
                }

                if (buttons_panel_view == "true") {
                    $(slide_chooser.children()[maxSliderss]).attr('class', 'slider_chooser_unactive');
                    $(slide_chooser.children()[currentSlidee]).attr('class', 'slider_chooser_active');
                }
            } else {
                $($(sliderss)[currentSlidee - 1]).attr('class', 'it');
                $($(sliderss)[currentSlidee]).attr('class', 'it slider_active');

                if (animation == "fadeto") {
                    $($(sliderss)[currentSlidee - 1]).css("opacity","0");// fix bug when changing image. When opacity 0 showing last one image on top.
                    $($(sliderss)[currentSlidee + 1]).css("opacity","0");// fix bug when changing image. When opacity 0 showing last one image on top.

                    $($(sliderss)[currentSlidee]).css("opacity","0");
                    $($(sliderss)[currentSlidee]).fadeTo( "slow" , 1, function() {
                        //action
                    });
                } else if (animation == "slide") {
                    if (parseInt($(slid).css("right").replace(/[^-\d\.]/g, '')) != parseInt(full_block_slide_width)) {
                        $($(slid)).animate({"right": '+=' + block_slide_width });
                    }
                } else if (animation == "none") {
                    //none
                }

                if (buttons_panel_view == "true") {
                    $(slide_chooser.children()[currentSlidee - 1]).attr('class', 'slider_chooser_unactive');
                    $(slide_chooser.children()[currentSlidee]).attr('class', 'slider_chooser_active');
                }
            }

            if (is_background == true) {
                if (currentSlidee == 0) {
                    $(slid).css('background-color', '#23B0B7');
                } else if (currentSlidee == 1) {
                    $(slid).css('background-color', '#2B689E');
                } else if (currentSlidee == 2) {
                    $(slid).css('background-color', '#BBD8A2');
                } else if (currentSlidee == 3) {
                    $(slid).css('background-color', '#B1DCE2');
                } else if (currentSlidee == 4) {
                    $(slid).css('background-color', '#95C9DF');
                }
            }

            currentSlidee++;
            slided_counter++;
        }

        function setUpEnvironment () {
            $(main_slider).css("width", block_slide_width);
            $(main_slider).css("height", block_slide_height);

            // BLOCK CONFIGS
            if (animation == "slide") {
                $(main_slider).css("overflow", "hidden");
                $(slid).css("width", "50000px");
                $(slid).css("position", "relative");
                $(slid).css("right", ((currentSlidee - 1) * block_slide_numb_width) + "px");

                $.each($(sliderss), function (key, val) {
                    $(val).css("width", block_slide_width);
                    $(val).css("height", block_slide_height);
                    $(val).css("float", "left");
                    $(val).css("position", "relative");
                    $(val).css("width", "none");
                    $(val).find('img').css("position", "relative");
                });
            } else {
                $(slid).css("width", block_slide_width);
                $(slid).css("height", block_slide_height);
            }

            //EACH SLIDERS
            if (is_slide_urls == "true") {
                $.each($(sliderss), function (key, val) {
                    var img = $(val).find('img');
                    var img_url = img.attr("data-onclick");

                    // SET ONCLICK EVENT ON IMAGE IF HAVE ONCLICK DATA URL
                    if (img_url) {
                        img.click(function () {
                            window.location = img_url;
                        });

                        img.css("cursor", "pointer");
                    } else {

                    }
                });
            }
        }

        function setUpButtonsPanel () {
            if (buttons_panel_view == "true") {
                var html = '<div class="slider_chooser_wrap" >';

                $.each(sliderss, function (key, val) {
                    if (val.className == "it slider_active") {
                        html += '<div class="slider_chooser_active" data-slide-id="' + key + '"></div>';
                    } else {
                        html += '<div class="slider_chooser_unactive" data-slide-id="' + key + '"></div>';
                    }
                });

                html += '</div>';
                $(slid).after(html);

                slide_chooser = $(main_slider).find('.slider_chooser_wrap');

                $(slide_chooser.children()).on('click', function () {
                    clearInterval(interval);

                    if (currentSlidee == 0) {
                        $($(sliderss)[currentSlidee]).attr('class', 'it');
                        $(slide_chooser.children()[currentSlidee]).attr('class', 'slider_chooser_unactive');
                    } else {
                        $($(sliderss)[currentSlidee - 1]).attr('class', 'it');
                        $(slide_chooser.children()[currentSlidee - 1]).attr('class', 'slider_chooser_unactive');
                    }

                    currentSlidee = this.getAttribute('data-slide-id');
                    changeSlide();

                    interval = setInterval(changeSlide, speed);
                });
            }
        }

        function setUpCurrentSlide () {
            
            $.each(sliderss, function (key, val) {
                if (val.className == "it slider_active") {
                    currentSlidee = key + 1;
                }else if (animation == "fadeto") {
                   $(val).css('opacity', '0');
                }
            });
        }
    };
})(jQuery, window, document);